// Put the whole content of file "stdio.h" here before compiling
// Use <,> for system headers
// Searches file in predefined locations (include path)
#include <stdio.h>

// Programmer defined headers
// search for "," headers in the current directory (where the file itself
// located) and then in location used for <,> (system) headers
#include "myheader.h"


// To protect header from multiple inclusion we use header guards 
// See 02_included_header.h
