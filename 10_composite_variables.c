struct Point
{
	int x; // member
	int y;
};

Point mypoint;
mypoint.x = 5;
mypoint.y = 8;

Point pinitialized = { 5, 7 };
Point pinit1 = { .x = 5, .y = 7 };

Point* pp = &mypoint;

// access members of a struct pointer
pp->x = 6; // (*pp).x = 6;

// Unnamed struct
struct
{
	const char* name;
	int value;
} my_unnamed_instance = { "Mystring", 4 };
