
// In a c program function main is mandatory
int main()
{
	// Function body
	printf("Hello world\n");

	if (!is_it_ok())
		return -5;

	return 0; // Everything is all right
}

// If you mind the parameters given in the command line use
//
// Note: multiple main functions are not allowed in a c program
// so this example should not compile
int main(int argc, char* argv[])
{
}
