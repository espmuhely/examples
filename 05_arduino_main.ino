// In arduino main file with .ino extension
// setup and loop is the two mandatory functions
// setup called only once after start
// loop called periodically after setup as frequently as possible
void setup()
{
}

void loop()
{
}

// ---------- end of file

// It's good to know that actually, arduino programs also have a
// main.c prewritten for us.
// Looks something like that:

#include "Arduino.h"

// Includes our ino file with setup, loop and other stuff
#include "Ourmain.ino"

void main()
{
	// setup system things
	setup_system();

	// call our setup function
	setup();

	while(true)
	{
		// Do some housekeeping (perhaps nothing)
		// in esp8266 can be the handling of tcp/ip sockets etc.
		housekeeping();
		
		// call our loop function
		loop();
	}
}

