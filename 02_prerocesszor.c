// Preprocessor directives starts with '#'

// A define
#define A 1

// Conditional: int a = 1; exists only if A is defined
#ifdef A
int a = 1;
#endif

// Always protect math expressions with brackets
#define MAGICNUM (15 + 2 * 23)
// Usage
a = MAGICNUM * 3;

// Macros with arguments (note the brackets around the operands)
#define plus(a,b) ((a) + (b))

plus(4 + 3, a/b);

// A more usefule example
#define min(X, Y)  ((X) < (Y) ? (X) : (Y))

int a = min(5 + 6*r, 8)

// debug macro with line numbers
// note escaped newline character at the end of the first line
// (macro defintion should be one line)
#define debug(msg) printf("in file:%s on line: %d message:%s\n", __FILE__, __LINE__, \
				  msg)
debug("Minden OK")