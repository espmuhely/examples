
// global variables (available during the execution of the program)
int a = 5;

char b; // implicitly initialized to zero

// This variable is available only inside this source file
static int my_filelocal;

// This function is avaiable only inside this module
static int mysetup()
{
}

void function_with_local_variables()
{
	// local variable, available during the execution of the function
	// or in a block ( {,} delimited
	char array[5];

	// local variables are not initialized.
	// Must be initialized before the first usage.
	// compiler warns if not initialized.
	int b = 5;

	{
		int c = 3; // available inside this block
		printf("%d", c);
	}

	while(true)
	{
		int d; // inside this block
	}
}

// global varibles inside a function or block
void function_with_static_variables()
{
	// initialized during program start
	// exists during the eecution of the program
	static int c = 5;
	
	c++;
}

// Heap variables

// new Create a variable on heap
int* a = new int;

// Deletes the variable pointed by a
delete a;

// In modern c++ smart pointers are used instead of normal pointers and new,
// delete -> to avoid memory leak
