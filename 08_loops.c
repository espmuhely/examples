// for
for(int i = 0; i < 5; i++)
{
}


// while
int b = 0;
while (b < 5)
{
	// executed while the condition is true
	printf("\d\n", b);
}

// do while
do
{
// Executed at least once
} while (b > 3);

