// A function the returns an int (number) and has no parameters.
int myfunc1()
{
	// Calculate and return some value here
	return 4;
}

// A function with no return value (void)
void myprocedure()
{
	// Do stuff here
	solve_the_universe();
}

// A function with 2 parameters and float return value
float my_complex_func(int a, float b, char* message)
{
	printf("parameters:%d, %f, %s\n", a, b, message);

	return .4;
}

// pass by value
void my_func_by_value(int a)
{
	a = 5; // Won't affect the caller
}
int f = 2;
my_func_by_value(f);


// pass by pointer
void my_func_by_pointer(int* a)
{
	if (a != NULL)
	{
		// *a dereferences the 'a' pointer
		// (refers to the variable passed to the function)
		*a = 5;
	}
}
int b = 2;
my_func_by_pointer(&b); // b will be 5 after this function call, '&' takes the pointer to b
my_func_by_pointer(NULL);


// pass by reference (only in C++)
void my_func_by_ref(int& a)
{
	// 'a' actually references the variable passed to the function
	a = 5;
}
int c = 2;
my_func_by_ref(c); // c will be 5 after calling this function


// const parameters
// Useful for more complex data parameters to avoid data copy
void my_conts(const ComplexType& a)
{
	int c = a.member;

	a.member = 5; // !!!!!! compiler error, a can not be changed by the function
}

ComplexType y;
my_conts(y);
