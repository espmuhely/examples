#ifndef __INCLUDED_HEADER_H__ // Could be any preprocessor variable
#define __INCLUDED_HEADER_H__

// Stuff that wont be defined twice even if this header is included more than
// once

// A global variable that will be defined in another object file
extern int myglobal;

// A function that is defined in another object file
extern void myfunc(int a, int b);

#endif
