// -----  numbers
// 8 bits = signed -127 - +126, unsigned 0 - 255
// 16 bits = unsigned 0 - 65535, signed ....
// 32 bits = unsigned 0 - 4.294.967.295, signed ....
// 64 bits = unsigned 0 - 4.294.967.295, signed ....

int a; // size depends on the processzor architecture at least 16 bits
unsigned int b; // unsigned version

char my_byte; // 8 bit signed
unsigned char my_ubyte; // 8 bit unsigned

short my_16bit;
unsigned short my_u16bit;

long my_32bit;
unsigned long my_u32bit;

long long my_64bit;
unsigned long long my_u64bit;

// floating point numbers
float my_float;
double my_dobleprecisionfloat;

// Arrays

int my_intarray[10]; // array with 10 integers

my_intarray[0] = 1; // references the first member
my_intarray[9] = 4; // references the last member

int my_size_determined_during_init[] = {
	5, 1, 3, 12
};

// pointers
int* pointer_to_an_integer; // an address points to an integer

int i;
pointer_to_an_integer = &i; // set it to point to 'i' variable

*pointer_to_an_integer = 5; // set the referenced integer to 5 (this case 'i'

// an array can also be used as a pointer
*my_intarray = 3; // Sets the first element to 3
*(my_intarray + 1) = 6; // sets the second element to 6


// c style strings (actually a character array with a closing 0 byte
char mychar[] = { 'a', 'l', 'm', 'a', 0 };
const char* my_string = "My string"; // statically allocated string, can not be changed


