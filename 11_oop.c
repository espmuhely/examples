// Object oriented programming (only in C++ not in C)

struct Point
{
	int x,y;

	Point() // construction called explicitly when created
	{
		x = y = 0;
	}

	Point(int xi, int yi) // nondefault constructor
	{
		x = xi;
		y = yi;
	}

	~Point() // Destructor called when the object destroyed
	{
		printf("destructed");
	}

	double distance_from_origo()
	{
		return sqrt(x*x + y*y);
	}
}

Point myp; // construction called implicitly

{
	Point myLocalp;
	printf("lenght:%f\n", myLocalp.distance_from_origo());
} // destructor called at this point

// Create object dynamically with non default constructor
Point myNonDefault(6, 7);

Point* myDynamic = new Point(5,6);
printf("lenght:%f\n", myDynamic->distance_from_origo());

// Classes
